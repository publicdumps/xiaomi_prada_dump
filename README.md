## prada-user 6.0.1 MMB29M V10.2.2.0.MCEMIXM release-keys
- Manufacturer: xiaomi
- Platform: msm8937
- Codename: prada
- Brand: Xiaomi
- Flavor: prada-user
- Release Version: 6.0.1
- Id: MMB29M
- Incremental: V10.2.2.0.MCEMIXM
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-GB
- Screen Density: 320
- Fingerprint: Xiaomi/prada/prada:6.0.1/MMB29M/V10.2.2.0.MCEMIXM:user/release-keys
- OTA version: 
- Branch: prada-user-6.0.1-MMB29M-V10.2.2.0.MCEMIXM-release-keys
- Repo: xiaomi_prada_dump


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
