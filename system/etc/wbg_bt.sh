#!/bin/bash

CMD=`getprop persist.sys.wbg.bt`
TESTITEM=`getprop persist.sys.wbg.bt.testitem`
PRO_CHANNEL=`getprop persist.sys.wbg.bt.channel`
PRO_PAYLOAD=`getprop persist.sys.wbg.bt.payload`
PRO_POWER=`getprop persist.sys.wbg.bt.power`
PRO_SLOT=`getprop persist.sys.wbg.bt.slot`
RX_DATA=`getprop persist.sys.wbg.bt.package`
LE_PACKAGE=`getprop persist.sys.wbg.bt.lepackage`


function BT_Config(){
	# set channel
	CHANNEL=$PRO_CHANNEL
	expr "$1=\"$CHANNEL\""
	# set pay_load
	if [ $PRO_PAYLOAD == "All0" ]; then
		PAY_LOAD="00"
		expr "$2=\"$PAY_LOAD\""
	elif [ $PRO_PAYLOAD == "All1" ]; then
		PAY_LOAD="01"
		expr "$2=\"$PAY_LOAD\""
	elif [ $PRO_PAYLOAD == "ZOZO" ]; then
		PAY_LOAD="02"
		expr "$2=\"$PAY_LOAD\""
	elif [ $PRO_PAYLOAD == "FOFO" ]; then
		PAY_LOAD="03"
		expr "$2=\"$PAY_LOAD\""
	elif [ $PRO_PAYLOAD == "Ordered" ] || [ $3 == "PRBS9" ]; then
		PAY_LOAD="04"
		expr "$2=\"$PAY_LOAD\""
	fi
	# set Slot
	if [ $PRO_SLOT == "DH1" ]; then
		SLOT="04"
		PAYLOAD_LENGTH_1="1B"
		PAYLOAD_LENGTH_2="00"
		expr "$3=\"$SLOT\""
		expr "$5=\"$PAYLOAD_LENGTH_1\""
		expr "$6=\"$PAYLOAD_LENGTH_2\""
	elif [ $PRO_SLOT == "DH3" ]; then
		SLOT="0B"
		PAYLOAD_LENGTH_1="B7"
		PAYLOAD_LENGTH_2="00"
		expr "$3=\"$SLOT\""
		expr "$5=\"$PAYLOAD_LENGTH_1\""
		expr "$6=\"$PAYLOAD_LENGTH_2\""
	elif [ $PRO_SLOT == "DH5" ]; then
		SLOT="0F"
		PAYLOAD_LENGTH_1="53"
		PAYLOAD_LENGTH_2="01"
		expr "$3=\"$SLOT\""
		expr "$5=\"$PAYLOAD_LENGTH_1\""
		expr "$6=\"$PAYLOAD_LENGTH_2\""
	elif [ $PRO_SLOT == "2DH1" ]; then
		SLOT="24"
		PAYLOAD_LENGTH_1="36"
		PAYLOAD_LENGTH_2="00"
		expr "$3=\"$SLOT\""
		expr "$5=\"$PAYLOAD_LENGTH_1\""
		expr "$6=\"$PAYLOAD_LENGTH_2\""
	elif [ $PRO_SLOT == "2DH3" ]; then
		SLOT="2A"
		PAYLOAD_LENGTH_1="6F"
		PAYLOAD_LENGTH_2="01"
		expr "$3=\"$SLOT\""
		expr "$5=\"$PAYLOAD_LENGTH_1\""
		expr "$6=\"$PAYLOAD_LENGTH_2\""
	elif [ $PRO_SLOT == "2DH5" ]; then
		SLOT="2E"
		PAYLOAD_LENGTH_1="A7"
		PAYLOAD_LENGTH_2="02"
		expr "$3=\"$SLOT\""
		expr "$5=\"$PAYLOAD_LENGTH_1\""
		expr "$6=\"$PAYLOAD_LENGTH_2\""
	fi
	# set power
	if [ $PRO_POWER == "Class1" ]; then
		POWER="09"
		expr "$4=\"$PAY_LOAD\""
	elif [ $PRO_POWER == "Class2" ]; then
		POWER="07"
		expr "$4=\"$PAY_LOAD\""
	elif [ $PRO_POWER == "Class3" ]; then
		POWER="05"
		expr "$4=\"$PAY_LOAD\""
	fi
}

function BT_LE_Config(){
	# set channel
	CHANNEL=$PRO_CHANNEL
	expr "$1=\"$CHANNEL\""
	# set pay_load
	if [ $PRO_PAYLOAD == "PRBS9" ]; then
		LE_PAY_LOAD="00"
		expr "$2=\"$LE_PAY_LOAD\""
	elif [ $PRO_PAYLOAD == "ZOZO" ]; then
		LE_PAY_LOAD="02"
		expr "$2=\"$LE_PAY_LOAD\""
	elif [ $PRO_PAYLOAD == "FOFO" ]; then
		LE_PAY_LOAD="01"
		expr "$2=\"$LE_PAY_LOAD\""
	fi
}

function HCI_Rest(){
	echo hcicmd 03 0C 00 > /data/brcmbt_fifo
	sleep 0.5
}

function Bluetooth_GetRxdata(){
	echo hcicmd 04 FC 01 02 > /data/brcmbt_fifo
	sleep 0.5

	HCI_BIT="0"
	# Capture final input hci command data
	/system/bin/ftm_tools/busybox tail -n 19 /data/bt_rx.txt > /data/rx_capture.txt
	# Take out RX hci data to count total bit
	/system/bin/ftm_tools/busybox sed -e '/\[0000\]   FF A3 04 1B/p' -e '1,/\[0000\]   FF A3 04 1B/d' /data/rx_capture.txt > /data/rx_hci.txt

	for i in 1 3 5 7 9;
	do
		HCI_RAW_DATA=$(/system/bin/ftm_tools/busybox printf "%d" $(/system/bin/ftm_tools/busybox awk 'NR=='$i'{ print "0x"$10 $9 $8 $7 } ' /data/rx_hci.txt))
		RESET_RAW_DATA=$(/system/bin/ftm_tools/busybox printf "%d" $(/system/bin/ftm_tools/busybox awk 'NR=='$i'{ print "0x"$14 $13 $12 $11 } ' /data/rx_hci.txt))
		RAW_DATA=`/system/bin/ftm_tools/busybox expr $HCI_RAW_DATA - $RESET_RAW_DATA`
		HCI_BIT=`/system/bin/ftm_tools/busybox expr $HCI_BIT + $RAW_DATA`
	done

	# Count total bit = TOTAL_HCI_BIT * Max_payload_length * 5 channels
	# Max_payload_length: DH1(27), DH3(183), DH5(339), 2DH1(54), 2DH3(367), 2DH5(679)
	if [ $PRO_SLOT == "DH1" ]; then
		TOTAL_HCI_BIT=`/system/bin/ftm_tools/busybox expr $HCI_BIT \* 27 \* 5`
	elif [ $PRO_SLOT == "DH3" ]; then
		TOTAL_HCI_BIT=`/system/bin/ftm_tools/busybox expr $HCI_BIT \* 183 \* 5`
	elif [ $PRO_SLOT == "DH5" ]; then
		TOTAL_HCI_BIT=`/system/bin/ftm_tools/busybox expr $HCI_BIT \* 339 \* 5`
	elif [ $PRO_SLOT == "2DH1" ]; then
		TOTAL_HCI_BIT=`/system/bin/ftm_tools/busybox expr $HCI_BIT \* 54 \* 5`
	elif [ $PRO_SLOT == "2DH3" ]; then
		TOTAL_HCI_BIT=`/system/bin/ftm_tools/busybox expr $HCI_BIT \* 367 \* 5`
	elif [ $PRO_SLOT == "2DH5" ]; then
		TOTAL_HCI_BIT=`/system/bin/ftm_tools/busybox expr $HCI_BIT \* 679 \* 5`
	else
		return_info "Can not find slot to count total bit"
		exit -1
	fi
	# setprop "total bit value" to persist.sys.wbg.bt.rxreport
	setprop persist.sys.wbg.bt.rxreport $TOTAL_HCI_BIT
	# delete capture rx_*.txt file
	rm /data/rx*
}

function Bluetooth_GetLeRxPacketCount(){
	echo hcicmd 1F 20 00 > /data/brcmbt_fifo
	sleep 0.5
	# Capture final input hci command data
	/system/bin/ftm_tools/busybox tail -n 6 /data/bt_rx.txt > /data/le_capture.txt
	# take out LE hci data
	/system/bin/ftm_tools/busybox sed -e '/\[0000\]   0E 06 01 1F 20/p' -e '1,/\[0000\]   0E 06 01 1F 20/d' /data/le_capture.txt > /data/le_hci.txt
	TOTAL_PACKET_BIT=$(/system/bin/ftm_tools/busybox printf "%d" $(/system/bin/ftm_tools/busybox awk 'NR==1{ print "0x"$9 $8 } ' /data/le_hci.txt))
	# setprop "total bit value" to persist.sys.wbg.bt.rxreport
	setprop persist.sys.wbg.bt.lerxreport $TOTAL_PACKET_BIT
	# delete capture le_*.txt file
	rm /data/le*
}

function Bluetooth_Module_on(){
	rm /data/brcmbt*
	sleep 0.5
	rm /data/bt_rx*
	sleep 0.5
	brcmbt --pipe /data/brcmbt_fifo > /data/bt_rx.txt &
	sleep 0.5
}

function Bluetooth_Module_off(){
	echo quit > /data/brcmbt_fifo
	sleep 0.5
	rm /data/brcmbt*
	sleep 0.5
	rm /data/bt_rx*
	sleep 0.5
}

function Bluetooth_Test(){
	if [ "$CMD" == "btstart" ]; then
		echo hcicmd 03 0C 00 > /data/brcmbt_fifo
		sleep 0.5
		echo hcicmd 09 10 00 > /data/brcmbt_fifo
		sleep 0.5
		echo hcicmd 1A 0C 01 03 > /data/brcmbt_fifo
		sleep 0.5
		echo hcicmd 05 0C 03 02 00 03 > /data/brcmbt_fifo
		sleep 0.5
		echo hcicmd 03 18 00 > /data/brcmbt_fifo
		sleep 0.5
	elif [ "$CMD" == "btreset" ]; then
		HCI_Rest
	fi
}

function Bluetooth_TxMode(){
	if [ "$CMD" == "btstart" ]; then
		BT_Config CHANNEL PAY_LOAD SLOT POWER PAYLOAD_LENGTH_1 PAYLOAD_LENGTH_2
		# command start
		#echo hcicmd 0B FC 04 01 1B 01 00 > /data/brcmbt_fifo
		#sleep 0.5
		echo hcicmd 03 0C 00 > /data/brcmbt_fifo
		sleep 0.5
		echo hcicmd 04 FC 15 04 $CHANNEL $CHANNEL $CHANNEL $CHANNEL $CHANNEL $PAY_LOAD $SLOT 00 $POWER 01 06 05 04 03 02 01 00 $PAYLOAD_LENGTH_1 $PAYLOAD_LENGTH_2 00 > /data/brcmbt_fifo
		sleep 0.5
	elif [ "$CMD" == "btreset" ]; then
		HCI_Rest
	fi
}

function Bluetooth_Rxmode(){
	if [ "$RX_DATA" == "false" ]; then
		if [ "$CMD" == "btstart" ]; then
			BT_Config CHANNEL PAY_LOAD SLOT POWER PAYLOAD_LENGTH_1 PAYLOAD_LENGTH_2
			# command start
			#echo hcicmd 0B FC 04 01 1B 01 00 > /data/brcmbt_fifo
			#sleep 0.5
			echo hcicmd 03 0C 00 > /data/brcmbt_fifo
			sleep 0.5
			echo hcicmd 04 FC 15 06 $CHANNEL $CHANNEL $CHANNEL $CHANNEL $CHANNEL 04 $SLOT 00 09 01 EE FF C0 88 00 00 00 $PAYLOAD_LENGTH_1 $PAYLOAD_LENGTH_2 00 > /data/brcmbt_fifo
			sleep 0.5
		elif [ "$CMD" == "btreset" ]; then
			HCI_Rest
		fi
	elif [ "$RX_DATA" == "true" ]; then
		Bluetooth_GetRxdata
	fi
}

function Bluetooth_LeTxMode(){
	if [ "$CMD" == "btstart" ]; then
		BT_LE_Config CHANNEL LE_PAY_LOAD
		# command start
		echo hcicmd 03 0C 00 > /data/brcmbt_fifo
		sleep 0.5
		echo hcicmd 1E 20 03 $CHANNEL 25 $LE_PAY_LOAD > /data/brcmbt_fifo
		sleep 0.5
	elif [ "$CMD" == "btreset" ]; then
		HCI_Rest
	fi
}

function Bluetooth_LeRxmode(){
	if [ "$LE_PACKAGE" == "false" ]; then
		if [ "$CMD" == "btstart" ]; then
			BT_LE_Config CHANNEL
			# command start
			echo hcicmd 03 0C 00 > /data/brcmbt_fifo
			sleep 0.5
			echo hcicmd 1D 20 01 $CHANNEL > /data/brcmbt_fifo
			sleep 0.5
		elif [ "$CMD" == "btreset" ]; then
			HCI_Rest
		fi
	elif [ "$LE_PACKAGE" == "true" ]; then
		Bluetooth_GetLeRxPacketCount
	fi
}

function Bluetooth_CW(){
	if [ "$CMD" == "btstart" ]; then
		# set channel
		BT_Config CHANNEL
		# command start
		echo hcicmd 03 0C 00 > /data/brcmbt_fifo
		sleep 0.5
		echo hcicmd 04 FC 09 05 $CHANNEL 07 04 01 00 00 00 00 > /data/brcmbt_fifo
		sleep 0.5
	elif [ "$CMD" == "btreset" ]; then
		HCI_Rest
	fi
}


case $TESTITEM in
	"enablebt")
			Bluetooth_Module_on
			;;
	"disablebt")
			Bluetooth_Module_off
			;;
	"enabletest")
			Bluetooth_Test
			;;
	"enabletx")
			Bluetooth_TxMode
			;;
	"enablerx")
			Bluetooth_Rxmode
			;;
	"enableletx")
			Bluetooth_LeTxMode
			;;
	"enablelerx")
			Bluetooth_LeRxmode
			;;
	"enablecw")
			Bluetooth_CW
			;;
esac
