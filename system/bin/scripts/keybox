#!/system/bin/sh
#
# FTM (Factory test mode) script.
# Run this script under FTM mode.
#

source /data/ftm_status
source /system/bin/scripts/ftm_common

usage() {
	echo "usage: $0 [OPTIONS]...[Parameter_1] [Parameter_2]"
	echo "Performs FTM ( $0 ) TEST with the necessary parameters."
	echo "KeyBox Write/IsValid"
	echo "OPTIONS:"
	echo "-h    Show this message"
	echo "Parameters:"
	echo "Parameter_1 Write/IsValid"
	echo "Parameter_2 key"
}

SUCCESS="0"
AES_KEY_LENGTH_WRONG="1"
OPENSSL_TRANSFER_WRONG="2"
KEYBOX_INSTALL_FAIL="3"
KEYBOX_IS_INVALID="4"
OPEN_KEYBOX_ADDR_FILE_FAIL="5"
ERR_READ_FP_NULL="6"
ERR_MOUNT_PARTITION_FAIL="10"

KeyBox_Result() {
	if [ "$ret" = "$SUCCESS" ]; then
		return_info
	elif [ "$ret" = "$AES_KEY_LENGTH_WRONG" ]; then
		return_info "AES_KEY_LENGTH_WRONG"
	elif [ "$ret" = "$OPENSSL_TRANSFER_WRONG" ]; then
		return_info "OPENSSL_TRANSFER_WRONG"
	elif [ "$ret" = "$KEYBOX_INSTALL_FAIL" ]; then
		return_info "KEYBOX_INSTALL_FAIL"
	elif [ "$ret" = "$KEYBOX_IS_INVALID" ]; then
		return_info "KEYBOX_IS_INVALID"
	elif [ "$ret" = "$OPEN_KEYBOX_ADDR_FILE_FAIL" ]; then
		return_info "OPEN_KEYBOX_ADDR_FILE_FAIL"
	elif [ "$ret" = "$ERR_READ_FP_NULL" ]; then
		return_info "ERR_READ_FP_NULL"
	elif [ "$ret" = "$ERR_MOUNT_PARTITION_FAIL" ]; then
		return_info "ERR_MOUNT_PARTITION_FAIL"
	fi
}

KeyBox_Write() {
	if [ "$_DEBUG" == "on" ]; then
		keybox_test -v -c KeyBoxWrite -p $1
	else
		keybox_test -c KeyBoxWrite -p $1
	fi

	ret=$?

	KeyBox_Result $ret
}

KeyBox_IsValid() {
	if [ "$_DEBUG" == "on" ]; then
		keybox_test -v -c KeyBoxIsValid
	else
		keybox_test -c KeyBoxIsValid
	fi

	ret=$?

	KeyBox_Result $ret
}

###
# Main body of script starts here
###

#Option parsing
while getopts ":h" OPTION ; do
	case $OPTION in
	h)  usage
		exit 0
		;;
	?)  echo "Illegal option: -$OPTARG"
		usage
		exit 1
		;;
	esac
done

#set watchdog for ftm
ftm_watchdog

#TIME Measure --- Start
START=$(time_start)

#convert parmater lower to upper case
parameter_1=$(case_converter $1)

#if you want to add debug message, use DEBUG ahead
DEBUG echo "$0 $1 $2"

if [ "$parameter_1" = "WRITE" ]; then
	KeyBox_Write $2
elif [ "$parameter_1" = "ISVALID" ]; then
	KeyBox_IsValid
else
	return_info "Illegal Parameter"
fi

#TIME Measure --- END
END=$(time_end)

#TIME Measure --- DIFF
DIFF=$(time_diff $END $START)
DEBUG echo "TIME DIFF $DIFF"
