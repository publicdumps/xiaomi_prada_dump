#!/system/bin/sh
#
# FTM (Factory test mode) script.
# Run this script under FTM mode.
#

source /data/ftm_status
source /system/bin/scripts/ftm_common
source /system/bin/scripts/sensor_common

usage() {
	echo "usage: $0 [OPTIONS]...[Parameter_1]"
	echo "Performs FTM ( $0 ) TEST with the necessary parameters."
	echo "ECompass ping/ID/Value"
	echo "OPTIONS:"
	echo "-h    Show this message"
	echo "Parameters:"
	echo "Parameter_1 ECompass_function"
}

device=$(getprop ro.boot.device)

ECOMPASS_DEVICE="/sys/devices/virtual/input/bmm050/value"
ECOMPASS_DEVICE_ID="/sys/devices/virtual/input/bmm050/chip_id"

SUCCESS="0"
FAILED="1"
AWK="busybox awk"

ECompass_ping() {
	sensor_path=$(sysfs_get_input_classpath  "bmm050")
	echo 1 > "/sys/devices/virtual/input/bmm050/enable"
	sleep 0.5
	if [ "$sensor_path" = "fail"  ]; then
		return_info "Can't connect to ecompass"
	else
		return_info
	fi
	echo 0 > "/sys/devices/virtual/input/bmm050/enable"
}

ECompass_ID() {
	echo 1 > "/sys/devices/virtual/input/bmm050/enable"
	sleep 0.5
	DATA=/data/sensor_ecompass_id_read.txt
	if [ -e $DATA ]; then
		rm -f $DATA
	fi
	touch $DATA
	chmod 664 "/data/sensor_ecompass_id_read.txt"
	cat $ECOMPASS_DEVICE_ID > $DATA

	if [ -s $DATA ]; then
		$AWK '{sum += $1} END {printf ("ID=%d\n", sum)}' $DATA
		return_info
	else
		return_info "Can't read Ecompass"
	fi
	echo 0 > "/sys/devices/virtual/input/bmm050/enable"
}

ECompass_Value() {
	echo 3 > "/sys/devices/virtual/input/bmm050/op_mode"
	echo 1 > "/sys/devices/virtual/input/bmm050/enable"
	sleep 0.5
	DATA=/data/sensor_ecompass_read.txt
	if [ -e $DATA ]; then
		rm -f $DATA
	fi
	touch $DATA
	chmod 664 "/data/sensor_ecompass_read.txt"
	cat $ECOMPASS_DEVICE > $DATA

	if [ -s $DATA ]; then
		$AWK '{sumx = $1; sumy = $2; sumz = $3} END {printf ("Ecompass=%d,%d,%d\n", sumx/16,sumy/16,sumz/16)}' $DATA
		return_info
	else
		return_info "Can't read Ecompass"
	fi
	echo 0 > "/sys/devices/virtual/input/bmm050/enable"
	echo 2 > "/sys/devices/virtual/input/bmm050/op_mode"
}

###
# Main body of script starts here
###

#Option parsing
while getopts ":h" OPTION ; do
	case $OPTION in
	h)  usage
		exit 0
		;;
	?)  echo "Illegal option: -$OPTARG"
		usage
		exit 1
		;;
	esac
done

#set watchdog for ftm
ftm_watchdog

#TIME Measure --- Start
START=$(time_start)

#convert parmater lower to upper case
parameter_1=$(case_converter $1)

#if you want to add debug message, use DEBUG ahead
DEBUG echo "$0 $1"

if	[ "$device" = "fag" ]; then
	if [ "$parameter_1" = "PING" ]; then
		ECompass_ping
	elif [ "$parameter_1" = "ID" ]; then
		ECompass_ID
	elif [ "$parameter_1" = "VALUE" ]; then
		ECompass_Value
	elif [ "$parameter_1" = "TEMPERATURE" ]; then
		return_info "NOT SUPPORT"
	elif [ "$parameter_1" = "MAGNETICSENSOR" ]; then
		return_info "NOT SUPPORT"
	else
		return_info "Illegal Parameter"
	fi
elif [ "$device" = "nbq" ] || [ "$device" = "vzw" ]; then
	if [ "$parameter_1" = "PING" ]; then
		return_info "NOT SUPPORT"
	elif [ "$parameter_1" = "ID" ]; then
		return_info "NOT SUPPORT"
	elif [ "$parameter_1" = "VALUE" ]; then
		return_info "NOT SUPPORT"
	elif [ "$parameter_1" = "TEMPERATURE" ]; then
		return_info "NOT SUPPORT"
	elif [ "$parameter_1" = "MAGNETICSENSOR" ]; then
		return_info "NOT SUPPORT"
	else
		return_info "NOT SUPPORT"
	fi
else
	return_info "NOT SUPPORT"
fi

#TIME Measure --- END
END=$(time_end)

#TIME Measure --- DIFF
DIFF=$(time_diff $END $START)
DEBUG echo "TIME DIFF $DIFF"
